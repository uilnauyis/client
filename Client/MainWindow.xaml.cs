﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static HttpClient _client = new HttpClient();
        private const string STORAGE_PATH = "files";

        public MainWindow()
        {
            Application.Current.DispatcherUnhandledException += HandleUncaughtException;
            CreateStorage();
            InitializeComponent();
            Refresh();
        }

        private void HandleUncaughtException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            this.Close();
            System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }

        public async void ListBoxItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            FileViewer fileViewer = new FileViewer();
            fileViewer.Show();
            IsEnabled = false;

            ListBoxItem itemObject = (ListBoxItem)sender;
            Item item = (Item)itemObject.Content;
            string fileName = item.FileName;
            byte[] fileContent;
            try
            {
                var response = await _client.GetAsync($"http://localhost:9001/cache/DownloadFile?fileName={fileName}");
                if (response.StatusCode != HttpStatusCode.OK)
                {
                }
                fileContent = await response.Content.ReadAsAsync<byte[]>();

                await SaveFileInClientStorage(fileName, fileContent);
                this.Dispatcher.Invoke(() =>
                {
                    fileViewer.Close();
                });
                Process.Start("notepad.exe", $"{STORAGE_PATH }/{fileName}");
                Process.Start($"{STORAGE_PATH }");
            }
            catch (Exception err)
            {
                this.Dispatcher.Invoke(() =>
                {
                    fileViewer.Close();
                });
            }
            IsEnabled = true;
        }

        private void Refresh_Button_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private async Task Refresh()
        {
            loader.Visibility = Visibility.Visible;
            avilableFiles.ItemsSource = new List<Item>();
            try
            {
                var response = await _client.GetAsync("http://localhost:9001/cache/ListFiles");
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    loader.Visibility = Visibility.Hidden;
                    return;
                }
                var responseContentStr = await response.Content.ReadAsStringAsync();
                var fileNames = JsonConvert.DeserializeObject<List<string>>(responseContentStr);

                List<Item> avilableFilesItems = new List<Item>();
                foreach (string fileName in fileNames)
                {
                    avilableFilesItems.Add(new Item
                    {
                        FileName = fileName,
                        IsLoading = Visibility.Hidden
                    });
                }
                avilableFiles.ItemsSource = avilableFilesItems;
            }
            catch (Exception err) { }
            loader.Visibility = Visibility.Hidden;
        }

        private async Task SaveFileInClientStorage(string fileName, byte[] content)
        {
            var filePath = $"{STORAGE_PATH }/{fileName}";

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            using (FileStream stream = File.Open(filePath, FileMode.CreateNew))
            {
                await stream.WriteAsync(content, 0, content.Length);
            }
        }

        public void CreateStorage()
        {
            Directory.CreateDirectory(STORAGE_PATH);
            DirectoryInfo di = new DirectoryInfo(STORAGE_PATH);
            foreach (var file in di.EnumerateFiles())
            {
                file.Delete();
            }
        }
    }

    class Item
    {
        public string FileName { get; set; }
        public Visibility IsLoading { get; set; }
    }
}
